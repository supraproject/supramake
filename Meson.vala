private string get_meson_exec (string path) throws Error{
	string contents;
	FileUtils.get_contents(@"$path/build/meson-info/intro-targets.json", out contents);

	var offset = contents.last_index_of("\"type\": \"executable\"");
	unowned string ptr = contents.offset(offset);
	ptr = ptr.offset(ptr.index_of("\"filename\""));
	ptr = ptr.offset(ptr.index_of_char('[') + 1);
	ptr = ptr.offset(ptr.index_of_char('"') + 1);
	var name = ptr[0:ptr.index_of_char('"', 13)];
	if (name._strip() == "")
		throw new OptionError.BAD_VALUE("No executable found in %s/build/meson-info/intro-targets.json", path);
	return name;
}

private string jump_to_mesonroot (string path) {
	string ptr = path;
	string? before = ptr;
	while (true) {
		before = ptr;
		ptr = Path.get_dirname(ptr);
		if (ptr == "/")
			break;
		if (ptr.has_suffix("/subprojects")) {
			continue;
		}
		else if (FileUtils.test(ptr + "/meson.build", FileTest.EXISTS)) {
			continue;
		}
		break;
	}
	return (owned)before;
}

public int run_meson(string _path, string []rules) throws Error {
	string path = _path;
	int wait;
	var cmd = rules[0] ?? "";
	if (cmd.has_suffix("help"))
	{
		printerr("Help de meson:\n");
		return 0;
	}

	path = jump_to_mesonroot(path);

	// check if there is a Makefile next to the meson.build 
	// Makefile have priority over meson.build
	if (FileUtils.test(path + "/Makefile", FileTest.EXISTS))
		return run_makefile(path, rules);

	printerr("Meson build detected !\n");
	if (FileUtils.test(path + "/build/build.ninja", FileTest.EXISTS) == false) {
		printerr("Meson setup !\n");
		Process.spawn_command_line_sync(@"meson setup $path/build $path/", null, null, out wait);
		if (wait != 0)
			throw new SpawnError.FAILED("Meson setup failed");
	}
	Process.spawn_command_line_sync(@"ninja -C $path/build", null, null, out wait);
	if (wait != 0)
		throw new SpawnError.FAILED("Ninja failed");
	int wait_status;
	unowned string all_av = Environment.get_variable("ARGS") ?? "";
	string name_exec = get_meson_exec(path);
	string []argvps;
	if (cmd == "run2") {
		Shell.parse_argv(@"valgrind --leak-check=full --show-leak-kinds=all $name_exec $all_av", out argvps);
		print ("%s\n", string.joinv(", ", argvps));

	}
	else if (cmd != "" && cmd != "run") {
		name_exec = name_exec[0:name_exec.last_index_of_char('/')] + @"/$cmd";
		Shell.parse_argv(@"$cmd $all_av", out argvps);
	}
	else  {
		Shell.parse_argv(@"$name_exec $all_av", out argvps);
	}
	Process.spawn_sync(null, argvps, null, GLib.SpawnFlags.CHILD_INHERITS_STDIN | SpawnFlags.SEARCH_PATH, null, null, null, out wait_status);
	return wait_status;
}
