public int run_makefile (string path, string []rules) throws Error {
	printerr("Makefile detected !\n");
	int wait_status;
	StrvBuilder builder = new StrvBuilder();
	builder.add("make");
	builder.addv(rules);
	Environment.set_variable("PWD", path, true);
	Process.spawn_sync(path, builder.end(), null, GLib.SpawnFlags.CHILD_INHERITS_STDIN | SpawnFlags.SEARCH_PATH, null, null, null, out wait_status);
	return (wait_status);
}
