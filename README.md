# Installation

## with meson.build

```bash
meson build --prefix=~/.local
ninja install -C build
```

## with Suprapack

```bash
meson build --prefix=$PWD/usr
ninja install -C build
suprapack install ./le_fichier_suprapack_generer.suprapack
```
