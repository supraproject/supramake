public const string SUPRAMAKE_PREFIX = "\033[31m[Supramake]\033[0m ";
[CCode (cname="strstr", cheader_filename="string.h")]
public extern unowned string strstr(string s1, string s2);

class Parser {
	private static bool clean = false;
	private static bool make_help = false;

	private const GLib.OptionEntry[] options = {
		{ "clean", '\0', OptionFlags.NONE, OptionArg.NONE, ref clean, "clear before running", null },
		{ "make-help", '\0', OptionFlags.NONE, OptionArg.NONE, ref make_help, "Show Makefile Help", null },
		{ null }
	};

	public Parser (string []args) throws Error {
		var opt_context = new OptionContext ("- OptionContext example");
		opt_context.set_help_enabled (true);
		opt_context.add_main_entries (options, null);
		opt_context.set_ignore_unknown_options(true);
		opt_context.set_summary("""
Arguments:  $(ARGS)
  You can pass arguments to the program you want to run. with -- before the arguments.
  Example: supramake run -- 1 2 3 4 5
  Will run the program with the arguments 1 2 3 4 5

  you need add the $(ARGS) in your Makefile rules
  Example:
  run:
	./my_program $(ARGS)
	
Compatibility:
  - Makefile
  - Meson build
  - Vala
  - C and C++
  - Python
  - Shell
  - Rust
  - Assembly
""");
		opt_context.parse (ref args);

		if (make_help) {
			Process.spawn_command_line_sync("make --help");
			Process.exit(0);
		}

		if (clean) {
			print("\033[2J\033[1;1H");
		}
	}
}

int   main(string []args) {
	string []export_args = {};
	for (int i = 0; i < args.length; i++)
	{
		if (args[i] == "--") {
			int n = i;
			++i;
			while (i < args.length)
			{
				export_args += args[i];
				++i;
			}
			args = args[0:n];
			break;
		}
	}
	int res = -1;
	try{
		new Parser(args);
		res = supramake(args, ref export_args);
	}catch (Error e) {
		printerr(e.message);
	}
	return res;
}
