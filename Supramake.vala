/**
  Search for a Makefile or a meson.build in the current directory and all parent directories
  If a Makefile is found, it will be executed
  If a meson.build is found, it will be executed
  If nothing is found, the program will be executed
*/
public int    supramake(string []args, ref string []export_args) throws Error {
	string? path = Environment.get_variable ("PWD");
	string HOME = Environment.get_variable ("HOME");
	string []rules = args[1:];

	Process.signal(GLib.ProcessSignal.INT, send_ctrl_c);
	prepare_env(ref export_args);

	while (HOME != path && path != "/tmp" && path != "/") {

		// Makefile part 
		if (FileUtils.test(path + "/Makefile", FileTest.EXISTS))
			return (run_makefile(path, rules));

		// Meson Build part 
		else if (FileUtils.test(path + "/meson.build", FileTest.EXISTS))
			return (run_meson(path, rules));
		
		else
			path = Path.get_dirname(path);
	}
	simple_exec(export_args);

	// NOTE : if supramake can't found a way to execute the program, it will return 42 
	return (42);
}

/**
	Set the env 'ARGS' to the command line arguments
*/
private void prepare_env(ref string []export_args) throws Error {
	var tmp_argv = Environment.get_variable("ARGS")?._strip() ?? "";
	if (tmp_argv != null && tmp_argv != "" && export_args.length == 0) {
		Shell.parse_argv(tmp_argv, out export_args);
	}
	if (export_args.length > 0)
		Environment.set_variable("ARGS", "\"" + string.joinv("\" \"", export_args) + "\"", true);
}

private void send_ctrl_c() {
	printerr(SUPRAMAKE_PREFIX + "Send Ctrl+C to child\n");
}
