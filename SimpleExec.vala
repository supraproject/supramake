[CCode (cname="system", cheader_filename="stdlib.h")]
private extern int system(string cmd);

private void exec(string ext, string []ARGS) {
	int status;
	string all_av = string.joinv(" ", ARGS);

	printerr(SUPRAMAKE_PREFIX + "try to compile %s project without Makefile\n", ext);
	switch (ext) {
		case "vala":
			status = system(@"valac *.vala --pkg=posix --pkg=gio-2.0 -X -O2 -X -w -o a.out && ./a.out $all_av");
			break;
		case "c":
			status = system(@"cc -g -Wall -Wextra *.c -o a.out && ./a.out $all_av");
			break;
		case "cpp":
			status = system(@"c++ -g -Wall -Wextra *.cpp -o a.out && ./a.out $all_av");
			break;
		case "sh":
			status = system("bash *.sh");
			break;
		case "py":
			status = system("python *.py");
			break;
		case "rs":
			status = system(@"rustc *.rs -o a.out && ./a.out $all_av");
			break;
		case "asm":
			status = system(@"nasm -f elf64 *.asm -o a.o && ld a.o -o a.out && ./a.out $all_av");
			break;
		case "s":
			status = system(@"nasm -f elf64 *.s -o a.o && ld a.o -o a.out && ./a.out $all_av");
			break;
		default:
			status = -1;
			break;
	}
	Process.exit(status);
}

public void simple_exec(string []export_args) throws Error {
	string path = Environment.get_variable ("PWD");
	unowned string name;
	var dir = Dir.open(path, 0);
	while ((name = dir.read_name ()) != null) {
		unowned string ext;
		ext = name.offset(name.last_index_of_char('.') + 1);
		if (   ext == "c"
			|| ext == "cpp"
			|| ext == "vala"
			|| ext == "py"
			|| ext == "sh"
			|| ext == "rs"
			|| ext == "asm"
			|| ext == "s"
			)
		{
			exec(ext, export_args);
		}
	}
}
